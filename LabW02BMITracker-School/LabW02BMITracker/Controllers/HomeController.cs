﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LabW02BMITracker.Models;

namespace LabW02BMITracker.Controllers
{
    public class HomeController : Controller
    {
        private const int MAX_CLIENTS = 20;
        private static readonly HealthProfile[] _clientProfiles = new HealthProfile[MAX_CLIENTS];

        public HomeController()
        {
            _clientProfiles[0] = new HealthProfile
            {
                Id = "102",
                FirstName = "Fred",
                LastName = "Flintstone",
                Gender = Gender.Male,
                BirthDay = 3,
                BirthMonth = 2,
                BirthYear = 1931,
                HeightInInches = 56,
                WeightInPounds = 201
            };
            _clientProfiles[1] = new HealthProfile
            {
                Id = "112",
                FirstName = "Lara",
                LastName = "Croft",
                Gender = Gender.Female,
                BirthDay = 2,
                BirthMonth = 14,
                BirthYear = 1991,
                HeightInInches = 65,
                WeightInPounds = 125
            };
            _clientProfiles[2] = new HealthProfile
            {
                Id = "109",
                FirstName = "Alex",
                LastName = "Fierro",
                Gender = Gender.Other,
                BirthDay = 6,
                BirthMonth = 1,
                BirthYear = 2001,
                HeightInInches = 61,
                WeightInPounds = 81
            };
        }

        public IActionResult Index()
        {
            return View(_clientProfiles);
        }

        [HttpPost]
        public IActionResult CreateProfile(HealthProfile profile)
        {
            int count = _clientProfiles.Count(s => s != null);
            //If there are MAX client profiles then we cant add this profile
            if(count != MAX_CLIENTS)
            {
                //Find the first location with a null entry and set it to profile
                for(int i  = 0; i < _clientProfiles.Length; i++)
                {
                    if(_clientProfiles[i] == null)
                    {
                        _clientProfiles[i] = profile;
                        break; //No need to go further
                    }
                }

            }
            return RedirectToAction("Index");
        }

        public IActionResult ViewProfiles()
        {
            return View(_clientProfiles);
        }

        public IActionResult ProfileDetails(string id)
        {
            var profile = _clientProfiles.FirstOrDefault( hp=> hp.Id == id);
            if(profile == null)
                {
                    return RedirectToAction("ViewProfiles");
                }
            return View(profile);
        }


        public IActionResult CreateProfile()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
