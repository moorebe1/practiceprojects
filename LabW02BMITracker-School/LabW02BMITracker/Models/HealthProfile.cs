﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LabW02BMITracker.Models
{
    public class HealthProfile
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public int BirthMonth { get; set; }
        public int BirthDay { get; set; }
        public int BirthYear { get; set; }
        public decimal HeightInInches { get; set; }
        public decimal WeightInPounds { get; set; }

        public int GetAgeInYears()
        {
            int CurrentYear = DateTime.Now.Year;
            int Age = CurrentYear - BirthYear;

            return Age;
        }

        public int GetMaxHeart()
        {
            int HeartRate = 220 - GetAgeInYears();

            return HeartRate;
        }

        public double GetTargetMinimumHeartRate()
        { 
            double MinimumHeart = (GetMaxHeart() * .5);
            return Math.Round(MinimumHeart, 0);
        }
        public double GetTargetMaximumHeartRate()
        {       
            double MaximumHeart = (GetMaxHeart() * .85);
            return Math.Round(MaximumHeart, 0);
        }

        public decimal GetBMI()
        {
            decimal BMI = (((WeightInPounds * 703) / (decimal)Math.Pow((double)HeightInInches, 2)));
            return Decimal.Round(BMI, 2);
        }

        public string GetBodyState()
        {
            string BodyState;
            if(GetBMI()< Convert.ToDecimal(18.5))
            {
                return BodyState = "Underweight";
            }
            else if(GetBMI() >= Convert.ToDecimal(18.5) && GetBMI() < Convert.ToDecimal(24.9))
            {
                return BodyState = "Normal";
            }
            else if(GetBMI() >= Convert.ToDecimal(25) && GetBMI() <= Convert.ToDecimal(29.9))
            {
                return BodyState = "Overweight";
            }
            else
            {
                return BodyState = "Obese";
            }
        }
    }
}
