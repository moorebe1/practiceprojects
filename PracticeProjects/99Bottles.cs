﻿using System;

namespace PracticeProjects
{
    class Program
    {
        static void Main(string[] args)
        {
            for(int i = 99; i > -1; i-- )
            {
                if(i == 0)
                {
                    Console.WriteLine("\nNo more bottles of beer on the wall, no more bottles of beer. \nGo to the store and buy some more, 99 bottles of beer on the wall.\n");
                    break;
                }
                if(i == 1)
                {
                    Console.WriteLine("1 bottle of beer on the wall, 1 bottle of beer.\nTake one down and pass it around, no more bottles of beer on the wall.");
                }
                else
                {
                    Console.WriteLine(i + " Bottles of Beer of on the Wall. \nTake One Down Pass It Around. \n" + (i - 1) + " Bottles of Beer of on the Wall.\n");
                }


            }
        }
    }
}
