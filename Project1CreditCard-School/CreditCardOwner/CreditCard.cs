﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Solution/Project:  Project1-CreditCard
// File Name: CreditCard.cs
// Description: Class that hold methods to validate the date and numbers of a credit card.  Will also 
//              set said card's issuer, i.e. Discover, Visa, etc.  Also holds construtor that takes in
//              card's numbers, name on card, and date; as well as basic setters and getters
//              for each accordingly.
// Course: CSCI 2210 - Data Structures
// Author: Brandon Moore, moorebe1@etsu.edu
// Created: September 23, 2017
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Project1
{
    public class CreditCard :  IComparable<CreditCard> , IEquatable<CreditCard>
    {
        private String CcNumbers
        {
            get
            {
                return CcNumbers;
            }
            set
            {
                this.CcNumbers = CcNumbers;
            }
        }

        private String CcName { get;  set; }   //Sets - Gets credit card name
        private String CcDate { get; set; }  //Sets - Gets credit card date
        private String Address { get; set; }  //Sets - Gets address
        private String Email { get; set; }   //Sets - Gets email
        private String Phone { get; set; }   //Sets - Gets phone
        private CardValues CcValue { get; set; }   //Sets - Gets credit card value type




        /// <summary>
        /// Initializes a new instance of the CreditCard class.
        /// </summary>
        /// <param name="ccNumbers">The cc numbers.</param>
        /// <param name="ccName">Name of the cc.</param>
        /// <param name="ccDate">The cc date.</param>
        public CreditCard(string ccName, string phone, string email, string ccNumbers, string ccDate)
        {

            Phone = phone;
            Email = email;
            CcNumbers = ccNumbers;
            CcName = ccName;
            CcDate = ccDate;
        }
       



        /// <summary>
        /// Will call from the enum class to set the card to the specific card type, based on the
        /// first few numbers. If no match is found, due to lack of numbers or too many numbers
        /// inputed it will return the card type as invalid.  
        /// Same goes if no specified match can be found for a "other" typed card.
        /// </summary>
        /// <param name="ccNumbers">Value that will be the user's inputed card numbers.  Used
        ///                         to find the card type.</param>
        /// <returns>Will return the card type.</returns>
        public static CardValues SetCardType(string ccNumbers)
        {
            CardValues ccValue;
            if (ccNumbers.Substring(0, 2) == "34" || ccNumbers.Substring(0, 2) == "37")
            {
                ccValue = CardValues.AMERICAN_EXPRESS;
            }
            else if (ccNumbers.Substring(0, 1) == "4")
            {
                ccValue = CardValues.VISA;
            }
            else if (ccNumbers.Substring(0, 2) == "51" || ccNumbers.Substring(0, 2) == "52" || ccNumbers.Substring(0, 2) == "53" || ccNumbers.Substring(0, 2) == "54" || ccNumbers.Substring(0, 2) == "55")
            {
                ccValue = CardValues.MASTERCARD;
            }
            else if(ccNumbers.Substring(0, 4) == "6011" || ccNumbers.Substring(0, 3) == "644" || ccNumbers.Substring(0, 2) == "65")
            {
                ccValue = CardValues.DISCOVER;
            }
            else if(ccNumbers.Length > 19 || ccNumbers.Length < 12)
            {
                ccValue = CardValues.INVALID;
            }
            else
            {
                ccValue = CardValues.OTHER;
            }

            return ccValue;
        }

        /// <summary>
        /// This method will take in the user's inputed card number and go
        /// through Luhn's algorithm to find out if they are valid numbers. 
        /// Will return true if the number's are valid.
        /// </summary>
        /// <param name="ccNumbers">User's card number that will be take in.</param>
        /// <returns>Returns true if the final number can be modded by 10 to equal 0.</returns>
        public static bool NumbersValid(string ccNumbers)
        {
          

            if (string.IsNullOrEmpty(ccNumbers) || ccNumbers.Length > 19 || ccNumbers.Length < 12)
            {
                return false;
            }


            //Luhn's algorithm that will go through the user's number input
            int sum = ccNumbers.Where((e) => e >= '0' && e <= '9')
                               .Reverse() //Reverses input 
                               .Select((e, i) => ((int)e - 48) * (i % 2 == 0 ? 1 : 2)) //Grabs every other, adds up if 
                                                                                       //two digits number occures after 
                                                                                       //multiplying
                               .Sum((e) => e / 10 + e % 10);  //Grabs the sum of the numbers


                     
            return sum % 10 == 0;
        }

        /// <summary>
        /// This method will take in the date given by the user and check if it is valid. 
        /// For it to be valid it will have to be greater than the current date.
        /// </summary>
        /// <param name="ccDate">The date given by the user to be checked.</param>
        /// <returns>Returns true if the date is greater then the current</returns>
        public static bool DateValid(string ccDate)
        {
            DateTime dateValue;

            //If statement that will compare date given to current date
            if (DateTime.TryParse(ccDate, out dateValue))
            {
                if (dateValue < DateTime.Now)
                {
                    return false;
                }

                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        } //end of dataValid

          public override string ToString()
        {
            String info =    "Name on Card: " + CcName
                           + "\nCard Owner Phone: " + Phone + "-Valid Phone Number: " + CreditCardOwner.IsValidPhone(Phone)
                           + "\nCard Owner Email: " + Email + "-Valid Email Number: " + CreditCardOwner.IsValidEmail(Email)
                           + "\nCard Number: " + CcNumbers 
                           + "\nCard Date: " + CcDate;
            return info;
        }  //end of ToString
        bool IEquatable<CreditCard>.Equals(CreditCard card)
        {
            return CcNumbers == card.CcNumbers;
        }//end of IEqutable<CreditCard>

        public override bool Equals(object obj)
        {
            if(obj == null)
            {
                return base.Equals(obj);
            }//end of if
            if(!(obj is CreditCard))
            {
                throw new ArgumentException("Parameter is not a credit card.");
            }//end of if
            else
            {
                return Equals(obj as CreditCard);
            }//end of else
        }//end of Equals

        public override int GetHashCode()
        {
            return CcNumbers.GetHashCode();  
        }//end of GetHasCode

        public int CompareTo(CreditCard other)
        {
            return this.CcNumbers.CompareTo(other.CcNumbers);
        }//end of CompareTo

        

    } //end of class
} //end of namespace
