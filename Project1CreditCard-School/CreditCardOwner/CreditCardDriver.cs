﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Solution/Project:  Project1-CreditCard
// File Name: CreditCardDriver.cs
// Description: Driver that will gather info about the user - name, email, phone, address.  Will go through
//              and validate the phone and email with method created in the CreditCardOwner class and print
//              true if so.  Will then ask the user if they wish to input a card - taking in the numbers, date
//              and name - and validate all but the name through methods created in the creditcard class.  It 
//              will then give the card a type depending on the first few numbers or lack of numbers, etc.
//              After info is taken the driver will print true or false on if the items are valid and then ask
//              the user if they wish to add another card to repeat the process.
// Course: CSCI 2210 - Data Structures
// Author: Brandon Moore, moorebe1@etsu.edu
// Created: October 23, 2017
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.IO;
using System.Windows.Forms;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Project1
{
    class CreditCardDriver
    {
        /// <summary>
        /// Driver that will make use of method's created in the other classes to validate the info
        /// given by to it by the user.
        /// </summary>
        public static void Main()
        {
            
            String phoneCard;  //Holds card owner's phone
            String emailCard;  //Holds card owner's email
            String choice;  //Choice for while loop
            String creditCardNumber;  //Holds user's card numbers
            String expDate;   //Holds user's card date
            String cardName;  //Holds user's card name
            String fixedString; //Will be used to hold a fixed creditCardNumber string
            String deletePosition;
            int pars; //Hold parsed string input
            int deletePars;//holds Delete Pars

            CreditCardList newList =  new CreditCardList(); ;// List to hold credit card info

            


            

     
            do //Repeats until user chooses 11
            {
                Console.WriteLine("\nWelcome \nWhat would you like to do? " +
                                    "\n1. Create a new empty CreditCardList " +
                                    "\n2. Import file " +
                                    "\n3. Add a Credit Card " +
                                    "\n4. Remove a Credit Card " +
                                    "\n5. Retrieve and display a CreditCard from position n in the list " +
                                    "\n6. Retrieve and display a CreditCard by its card number " +
                                    "\n7. Retrieve and display all CreditCards belonging to a specified person " +
                                    "\n8. Retrieve and display all non-expired valid credit cards " +
                                    "\n9. Sort Credit Cards by card number " +
                                    "\n10. Display all Credit Cards  " +
                                    "\n11. End program ");
                choice = Console.ReadLine();
                pars = Int32.Parse(choice);
                switch (pars)
                {

                    case 1:
                        {
                            newList = new CreditCardList();
                            Console.WriteLine("\nNew List Created!");

                            break;
                        }


                    case 2:
                        {
                            Input();
                          

                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("Name on the card: ");
                            cardName = Console.ReadLine();

                            Console.WriteLine("Phone of the card owner: ");
                            phoneCard = Console.ReadLine();

                            Console.WriteLine("Email of the card owner: ");
                            emailCard = Console.ReadLine();

                            Console.WriteLine("Card Numbers: ");
                            creditCardNumber = Console.ReadLine();
                            fixedString = Regex.Replace(creditCardNumber, @"[^\d]", ""); //Fixes string inputted, removing an non-digits

                            Console.WriteLine("Expiration Date(MM/YYYY): ");
                            expDate = Console.ReadLine();

                            CreditCard newCard = new CreditCard(cardName, phoneCard, emailCard, fixedString, expDate);

                            newList += newCard;

                            Console.WriteLine(newList);

                            break;
                        }
                    case 4:
                        {
                            Console.WriteLine("What position is the card you wish to delete: ");
                            deletePosition = Console.ReadLine();

                            deletePars = Int32.Parse(deletePosition);

                            newList = newList[deletePars - 1];



                            break;
                        }
                    case 5:
                        {


                            break;
                        }
                    case 6:
                        {


                            break;
                        }
                    case 7:
                        {


                            break;
                        }
                    case 8:
                        {


                            break;
                        }
                    case 9:
                        {


                            break;
                        }
                    case 10:
                        {


                            break;
                        }
                    case 11:
                        {
                            Console.WriteLine("Thank you for your use! ENDING");  //Ends the program

                            break;
                        }

                    default:
                        {
                            Console.WriteLine("Invalid choice entered.  Please choose from the menu.");

                            break;
                        }

                }  //end of switch


            } while (pars != 11);//end of while
          
        }     //end of Main

        
        private static void Input()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = Application.StartupPath + @"..\CreditCardData";
            dlg.Title = "Select the input file to be processed.";
            dlg.Filter = "text files (*.txt)|text files(*.text)|*.text|all files|*.*";

            if(dlg.ShowDialog() == DialogResult.Cancel)
            {
                MessageBox.Show("File Open canceled by user. Program exited.");
                Application.Exit();
            }


        }//end of Input() method
    }  //end of class

}  //end of Project 1
