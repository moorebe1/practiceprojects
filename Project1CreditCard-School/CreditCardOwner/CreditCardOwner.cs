﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Solution/Project:  Project1-CreditCard
// File Name: CreditCardOwner.cs
// Description: Class that holds information about the user, ie. name, address, email, phone.  In 
//              this class will also be two methods that will validate if the user has inputted a valid
//              phone number and email. Also holds a constructor that takes in the users info, as well as
//              basic setters and getters for each.
// Course: CSCI 2210 - Data Structures
// Author: Brandon Moore, moorebe1@etsu.edu
// Created: September 23, 2017
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;


namespace Project1
{
    class CreditCardOwner
    {
        private String Name { get; set; }  //Sets - Gets name
        private String Address { get; set; }  //Sets - Gets address
        private String Email { get; set; }   //Sets - Gets email
        private String Phone { get; set; }   //Sets - Gets phone
       
       




        /// <summary>
        /// Determines whether the given email from the user is valid based off of the regex used.
        /// If nothing is inputted or the user does that give a valid email false will be returned.
        /// </summary>
        /// <param name="Email">The email given by the user.</param>
        /// <returns>
        ///   True if a valid email; otherwise, false.
        /// </returns>
        public static bool IsValidEmail(string Email)
        {
           
                if (string.IsNullOrEmpty(Email))
                {
                    return false;
                }

                var correctEmail = new Regex(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*" + "@" + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$");
                return correctEmail.IsMatch(Email);

        }


        /// <summary>
        /// Determines whether the phone number given by the user is valid based off of the regex used.
        /// False will be returned if empty or does not match.
        /// </summary>
        /// <param name="Phone">The phone.</param>
        /// <returns>
        ///   True if it is a valid phone number; otherwise, false.
        /// </returns>
        public static bool IsValidPhone(string Phone)
        {

                if (string.IsNullOrEmpty(Phone))
                {
                    return false;
                }

                var correctPhone = new Regex(@"[0-9]{3}[0-9]{3}[0-9]{4}");
                return correctPhone.IsMatch(Phone);
 
        }                                             
    }
}
   