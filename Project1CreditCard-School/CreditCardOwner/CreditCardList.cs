﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Solution/Project:  Project2-CreditCard
// File Name: CreditCardList.cs
// Description: 
// Course: CSCI 2210 - Data Structures
// Author: Brandon Moore, moorebe1@etsu.edu
// Created: October 2, 2017
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project1

{
    class CreditCardList
    {
        private List<CreditCard> cardList;//list that holds all of the credit cards
        private CreditCard Count { get; }//gets the amount of creditcards inside of the list
        private bool SaveNeeded { get; }


        public CreditCardList()
        {
            cardList = new List<CreditCard>();
        }//end of default
        public CreditCardList(String fileName)
        {
            StreamReader rdr = null;
            try
            {
                rdr = new StreamReader(fileName);
                while(rdr.Peek () != -1)
                {
                    String[] fields = rdr.ReadLine().Split('|');
                    CreditCard newCard = new CreditCard(fields[0], fields[1], fields[2], fields[3], fields[4]);
                    cardList.Add(newCard);
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
                return;
                    
            }
            finally
            {
                if(rdr != null)
                {
                    rdr.Close();//closes the file
                }
            }

            
        }//end of default


        public static CreditCardList operator + (CreditCardList addOperator, CreditCard addCard)
        {
            addOperator.cardList.Add(addCard);
            return addOperator;
        }//end of operator +

        public static CreditCardList operator - (CreditCardList removeOperator, int position)
        {
            if (position > 0 && position <= removeOperator.cardList.Count)
            {
                removeOperator.cardList.RemoveAt(position - 1);
            }

            return removeOperator;
        }//end of operator - 

        public CreditCard this[int position]
        {
            get
            {
                if(position > 0 && position <= cardList.Count)
                {
                    return cardList[position];
                }//end of if
                else
                {
                    return null;
                }//end of else
            }//end of get
        } //end of indexer

        public void SortListByNumbers()
        {
            
        }

        public List<CreditCard> RetrieveNonExpired()
        {
            List<CreditCard> nonExpiredList = new List<CreditCard>();
            CreditCard cardDate = new CreditCard();
            for (int i = 0; i < cardList.Count; i++)
            {
                if ((CreditCard.DateValid(cardDate.CcDate) == true))
                {
                    nonExpiredList.Add(cardList.ElementAt(i));
                }
            }
            return nonExpiredList;
        }


        public List<CreditCard> RetrieveCardsByName (String name)
        {
            List<CreditCard> nameList = new List<CreditCard>(); //New list to hold/return list of "name" owned credit cards
            for(int i = 0; i < cardList.Count; i++)
            {
                if (cardList.ElementAt(i).CcNames.Equals(name) == true) //Checks if the name at position i is equal to given name
                {
                    nameList.Add(cardList.ElementAt(i));//Adds credit card to the list 
                }
            }
            return nameList;  //Returns all credit cards of that user
        }      

         
       
    }//end of class
}//end of namespace  