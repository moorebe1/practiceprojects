﻿///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Solution/Project: Project1-CreditCard
// File Name: CardValues.cs
// Description: List of card values that will be issued to a card if they meet the specific requirements
//              for that card brand.
// Course: CSCI 2210 - Data Structures
// Author: Brandon Moore, moorebe1@etsu.edu
// Created: September 23, 2017
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{   /// <summary>
    ///List of enums that will be used later in the program by the credit card class
    /// </summary>
    public enum CardValues
    {
        INVALID,
        VISA,
        MASTERCARD,
        AMERICAN_EXPRESS,
        DISCOVER,
        OTHER
    }
}
